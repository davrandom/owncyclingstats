# Open Sport Stats

## Goals

The aim of Open Sport Stats is to create markdown (MD) text files from gpx files.
These MD files will contain some statistics calculated over the gpx data,
similar to what is done by training applications (such as Strava, Endomondo, etc).
The generated files should be general enough to be plugged into the
most common static website generators, such as Jekyll, Pelican, etc.

The goal is to create an open-source framework to analise training data
and rely on other open-source frameworks for visualisation and navigation.


## Pillars

There shall be no databases!

Everything should be designed to be static.
Text files and proper folder hierarchy have to be sufficient.

### Parts that compose the whole

* {0.1} **stats** (pygpxstats)
* {_dev} **ui** \[_common, _desktop, _mobile\] (enaml, enaml-native)


## Examples and Tests

Tests should be simple and clear. They should serve also as minimal examples for someone that wants to understand and hack the code.


## Dependancies
```
gpxpy (parsing gpx)
fitparse (parsing fit files)
numpy (arrays - I want to remove this)
matplotlib (plots - I want to remove this in favour of bokeh)
geopy (calculate distances)
jekyll (blog)
folium (maps)
bokeh (web plots)
pandas (data structures)
```

Python:
```
python3 -m pip install gpxpy numpy matplotlib geopy folium fitparse
```


## Usage
```
python3 stats/pygpxstats.py gpx_filename.gpx
```

or read the help:
```
python3 pygpxstats.py -h
```

if you have jekyll intialized in "reports" folder, you can do
```
python3 stats/pygpxstats.py gpx_files/Commute_to_work.gpx -o reports/_posts/
```


# Examples for rendering
Rendering of the analysis output could be done via static website generators
such as Jekyll or Pelican.

We will (probably) provide a different repos for the most common configuration
of the mentioned website generators to work in conjunction to Open Sport Stats.


## Rendering via Jekyll
**To be Done**

* **reports** (jekyll)

"reports" is a jekyll blog:

```
jekyll new reports
```


## Rendering via Pelican
It has a simpler installation than Jekyll. 
My feeling is that is much less used than jekyll.

