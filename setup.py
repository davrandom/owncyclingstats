import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="osstats",
    version="0.0.1",
    author="BrutSalvadi",
    author_email="davrandom@gmail.com",
    description="Small Sport Stats collection of scripts that generates statistics from gpx or fit files"
                " that can be used in conjunction to static site generators, such as Jekyll and Pelican.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/davrandom/owncyclingstats/src/master/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: AGPLv3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
