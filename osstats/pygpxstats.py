#!/bin/python3

import osstats.parse_logs as pa
import osstats.logs_statistics as st
import osstats.output_stats as op

import os
from argparse import ArgumentParser
import gzip
import shutil
from datetime import datetime


force_analysis = True


def parse_and_write_plus_plot(gpslog_file_name, out_path=None, static_site_type='jekyll'):
    """
    Parses the input file and writes a set of files, such as markdown, png and html.
    The function returns two summaries of the activities, a total and a "moving" one in form of dictionaries.

    :param gpslog_file_name: input data in form of a gps log file.
    :param out_path: path where to write the output files.
    :return: dictionaries total_summary, moving_summary.
    """

    filename = os.path.split(gpslog_file_name)[-1]
    name, extension = os.path.splitext(filename)

    # Process file based on extension
    if extension == '.gpx':
        prsr = pa.GpxParserElementTree(gpslog_file_name)
    elif extension == '.fit':
        # prsr = pa.FitParser(gpslog_file_name)
        prsr = pa.FitParserAgain(gpslog_file_name)
    else:
        raise ValueError("Can't process this filetype %s. I can only process .gpx and .fit" % extension)

    if not out_path:
        out_path = name + '_files'

    is_out_path_existent = os.path.isdir(out_path)
    if not is_out_path_existent:
        os.makedirs(out_path)

    point = prsr.get_current_point()

    if point == 'eof':
        return False, False

    list_of_variables = prsr.list_of_variables
    initial_date = point['date']

    calendar_path = 'calendar'
    source, destination = make_calendar_folder_hierarchy(initial_date, gpslog_file_name, calendar_path)
    abs_activity_name, gpxextension = os.path.splitext(destination)

    is_activity_summary = is_activity_summary_existent(abs_activity_name+'.txt')
    if is_activity_summary and not force_analysis:
        prsr.close_file()
        return False, False

    der = st.InstantDerivedQuantities(point)
    der.push_new_observable_list(point)  # point zero (you don't calculate stuff here)
    point = prsr.get_next_point()  # here we go!

    t0 = point['date'].timestamp()

    data = st.Accumulate()
    data.set_initial_timestamp(t0)
    data.name = name

    counter = 1
    while True:
        if point == 'eof':
            break
        # save current data
        data.push_original_data(point)

        # calculate RT stuff
        der.push_and_calculate(point)

        # save derived quantities
        data.push_derived_quantities(der)

        point = prsr.get_next_point()
        counter += 1

    prsr.close_file()

    # output modules
    total_summary, moving_summary, pauses = op.summary(data)  # get stats summary
    op.figures_default_set(data, out_path)  # spit some plots

    op.print_dictionary(total_summary)
    op.print_dictionary(moving_summary)

    op.accumulate_to_pandas(data, out_path=out_path)

    # write summary to calendar folder structure
    op.write_dictionary_to_txt_file([total_summary, moving_summary], abs_activity_name+'.txt')
    # readdict = op.read_txt_file_to_dictionary(abs_activity_name+'.txt')

    # write summary to common out_path
    op.write_dictionary_to_txt_file([total_summary, moving_summary], os.path.join(out_path, 'summary.txt'))

    # post header
    head_md_name = "%s-%s-%s-" % (initial_date.year, initial_date.month, initial_date.day)
    mdfile = os.path.join(out_path, head_md_name+name+'.md')
    md = op.WriteToMarkdownFile(mdfile)

    if static_site_type == 'jekyll':
        md.header_jekyll(title=prsr.current_track_name, category='mtb collserola')
    elif static_site_type == 'pelican':
        post_title = prsr.current_track_name+'_'+head_md_name
        md.header_pelican(title=post_title, date=datetime.fromtimestamp(data.t0), category='mtb collserola')
    else:
        print("No markdown output selected.")

    # map
    if any(data.lat_list):
        map_html = op.gpx_map(data, out_path=out_path)
        # map_html = op.gpx_map(data)
        md.write_this('\n[//]: # "html code for the map"\n')
        for line in map_html:
            md.write_this(line)
            md.write_this("\n")
        md.write_this("\n")

    # tables
    md.write_this('\n[//]: # "tables"\n\n')
    total_table = md.make_table_out_of_dictionary(total_summary)
    md.write_this(total_table)
    md.write_this("\n")
    moving_table = md.make_table_out_of_dictionary(moving_summary)
    md.write_this(moving_table)

    # bokeh plots
    plot_string = op.bokeh_figures_default_set(data, out_path)
    md.write_this('\n[//]: # "html code for the plots"')
    md.write_this(plot_string)

    md.close()

    return total_summary, moving_summary


def make_calendar_folder_hierarchy(initial_date, gpx_absolute_filename, calendar_path):
    """
    Creates symlinks to the activities in a hierarchy of folders that follows this time structure:
    years -> months -> days.

    :param initial_date:
    :param gpx_absolute_filename:
    :param calendar_path:
    :return:
    """

    # Check types: initial_date has to be a datetime instance
    if type(initial_date) is not datetime:
        raise TypeError("initial_date should be a datetime instance.")

    filename = gpx_absolute_filename
    fn = os.path.split(filename)[-1]
    name, extension = os.path.splitext(fn)

    out_path = calendar_path
    is_out_path_existent = os.path.isdir(out_path)
    if not is_out_path_existent:
        os.makedirs(out_path)

    year_path  = os.path.join(out_path, str(initial_date.year))
    month_path = os.path.join(out_path, str(initial_date.year), '%02d' % initial_date.month)
    day_path   = os.path.join(out_path, str(initial_date.year), '%02d' % initial_date.month, '%02d' % initial_date.day)

    if not os.path.isdir(year_path):
        os.makedirs(year_path)

    if not os.path.isdir(month_path):
        os.makedirs(month_path)

    if not os.path.isdir(day_path):
        os.makedirs(day_path)

    # here (this folder)
    pwd = os.getcwd()
    # create symlink to original file in calendar structure
    src = os.path.join(pwd, filename)
    des = os.path.join(pwd, day_path, fn)
    is_symlink_exists = os.path.islink(des)

    # to deal with symlinks permissions in windows
    # check if symlinks are allowed
    os_symlink = getattr(os, "symlink", None)
    if callable(os_symlink):
        if not is_symlink_exists:
            # make link
            os.symlink(src, des)
    else:
        # otherwise make a copy
        shutil.copy(src, des)

    return src, des


def is_activity_summary_existent(txt_absolute_filename):
    is_activity_file = os.path.isfile(txt_absolute_filename)
    return is_activity_file


if __name__ == "__main__":
    """ When run as script, pygpxstats processes gpx and fit files.
    The script produces a set of output files, such as:
     - a markdown summary (to be used in conjunction to external software)
     - some png plots (matplotlib)
     - some html plots (bokeh)
     - some maps (folium)
     """

    parser = ArgumentParser(description="Parse gpx and create graphs and stats")
    parser.add_argument('gpxfile', metavar='gpxfile', type=str, help="Path and name of your gpx file")
    parser.add_argument('-o', metavar='output_destination', type=str, help="Destination path for generated output", default='.')
    parser.add_argument('-w', metavar='static_site_idx', type=int, help="Integer identifying the type of static web builder used: 0 for jekyll, 1 for pelican.", default=0)

    args = parser.parse_args()

    gpx_file_name = args.gpxfile
    output_dest = args.o

    static_site_type_dictionary = {0: 'jekyll', 1: 'pelican'}
    static_site_type = static_site_type_dictionary[args.w]

    is_gpx_file_name_isfile = os.path.isfile(gpx_file_name)
    is_gpx_file_name_isdir  = os.path.isdir(gpx_file_name)
    if is_gpx_file_name_isfile:
        # if gpx_file_name is_file then run once parser
        filename = os.path.split(gpx_file_name)[-1]
        name, extension = os.path.splitext(filename)
        updated_output_dest = os.path.join(output_dest, name + '_files')

        total_summary, moving_summary = parse_and_write_plus_plot(gpx_file_name, updated_output_dest, static_site_type)

    elif is_gpx_file_name_isdir:
        # if gpx_file_name is_directory the go through all the gpx files in the dir
        # parse_and_write_plus_plot(gpx_file_name, output_dest)
        list_of_files = os.listdir(gpx_file_name)
        for subfile in list_of_files:
            name, extension = os.path.splitext(subfile)

            # uncompress activities
            if extension == ".gz":
                if name not in list_of_files:
                    with gzip.open(os.path.join(gpx_file_name, subfile), 'rb') as f_in:
                        with open(os.path.join(gpx_file_name, name), 'wb') as f_out:
                            shutil.copyfileobj(f_in, f_out)
                            print("Unzipped %s." % subfile)

                            subfile = name
                            name, extension = os.path.splitext(subfile)
                else:
                    continue

            if extension == ".gpx" or extension == ".fit":
                file_to_process = os.path.join(gpx_file_name, subfile)

                filename = os.path.split(subfile)[-1]
                name, extension = os.path.splitext(filename)
                updated_output_dest = os.path.join(output_dest, name + '_files')

                try:
                    total_summary, moving_summary = parse_and_write_plus_plot(file_to_process, updated_output_dest)
                except Exception:
                    print("Failed to parse %s file." % subfile)
                    # raise ValueError("debug me")
                    pass

    else:
        raise ValueError("Your gpxfile is wrong and can't be processed.")
