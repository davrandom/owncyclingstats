import gpxpy
import gpxpy.gpx
import fitparse as fp
import pytz
from copy import copy


class DataContainer:
    def __init__(self):
        self.list_of_variables = {'lat': False, 'lon': False, 'ele': False, 'date': False,
                                  'hr': False, 'cad': False, 'atemp': False, 'grade': False,
                                  'speed': False, 'dist': False}


class GpxParserElementTree(DataContainer):

    def __init__(self, gpx_file_name):
        super().__init__()

        # TODO generalize namespace :P
        self.namespace = {'web': 'http://www.topografix.com/GPX/1/1',
                          'garmin': 'http://www.garmin.com/xmlschemas/TrackPointExtension/v1'}

        print("Start parsing %s ..." % gpx_file_name)

        self.gpx_file = open(gpx_file_name, 'r')
        gpx = gpxpy.parse(self.gpx_file)

        self.current_track_idx = 0
        self.current_segment_idx = 0
        self.current_point_idx = 0

        try:
            self.tracks = gpx.tracks
            self.total_number_of_tracks = len(self.tracks)

            self.current_track = self.tracks[self.current_track_idx]
            self.current_track_name = self.current_track.name

            self.segments = self.current_track.segments
            self.current_segment = self.segments[self.current_segment_idx]
            self.points = self.current_segment.points
            self.current_point = self.points[self.current_point_idx]
        except ValueError:
            raise ValueError("File is malformed.")

        self.number_of_segments_for_current_track = len(self.segments)
        self.number_of_points_for_current_segment = len(self.points)

    def close_file(self):
        self.gpx_file.close()

    def _update_track_info(self):
        self.current_track = self.tracks[self.current_track_idx]
        self.current_track_name = self.current_track.name
        self.segments = self.current_track.segments
        self.points = self.current_segment.points
        self.number_of_segments_for_current_track = len(self.segments)

    def _update_segment_info(self):
        self.current_segment = self.segments[self.current_segment_idx]
        self.number_of_points_for_current_segment = len(self.points)

    def _update_point_info(self):
        self.current_point = self.points[self.current_point_idx]

    def get_current_point(self):
        # and return point information
        lat = float(self.current_point.latitude)
        lon = float(self.current_point.longitude)

        if lat: self.list_of_variables['lat'] = True
        if lon: self.list_of_variables['lon'] = True

        ele = 0
        date = 0
        hr = 0
        cad = 0
        atemp = 0

        ele = self.current_point.elevation
        if ele is not None:
            self.list_of_variables['ele'] = True

        date = self.current_point.time
        if date is not None:
            self.list_of_variables['time'] = True

        extension = self.current_point.extensions
        if (type(extension) is list) and extension:
            has_hr = extension[0].find('garmin:hr', self.namespace)
            if has_hr is not None:
                hr = int(has_hr.text)
                self.list_of_variables['hr'] = True
            has_cad = extension[0].find('garmin:cad', self.namespace)
            if has_cad is not None:
                cad = int(has_cad.text)
                self.list_of_variables['cad'] = True
            has_temp = extension[0].find('garmin:atemp', self.namespace)
            if has_temp is not None:
                atemp = int(has_temp.text)
                self.list_of_variables['atemp'] = True

        return {'lat': lat, 'lon': lon, 'ele': ele, 'date': date, 'hr': hr, 'cad': cad, 'atemp': atemp, 'speed': False, 'dist': False}
        # fixme I don't like this. temporary // return an instance of DataContainer

    def get_next_point(self, verbose=False):
        self.current_point_idx += 1

        if self.current_point_idx >= self.number_of_points_for_current_segment:
            # if you reached the last point in segment, reset points counter
            self.current_point_idx = 0
            # and increment segments counter
            self.current_segment_idx += 1

        if self.current_segment_idx >= self.number_of_segments_for_current_track:
            # if you reached the last segment in the track, reset segments counter
            self.current_segment_idx = 0
            # and increment track counter
            self.current_track_idx += 1
        else:
            self._update_segment_info()

        if self.current_track_idx >= self.total_number_of_tracks:
            # if you reached the last track in the gpx file, then you are done
            print('Reached end of file.')
            return 'eof'
        else:
            # otherwise update all the information
            self._update_track_info()

        self._update_point_info()
        if verbose:
            print('track: %d/%d segment: %d/%d point: %d/%d' % (self.current_track_idx+1, self.total_number_of_tracks,
                                                                self.current_segment_idx+1, self.number_of_segments_for_current_track,
                                                                self.current_point_idx+1, self.number_of_points_for_current_segment))
        current = self.get_current_point()
        return current


class FitParser(DataContainer):
    def __init__(self, fit_file_name):
        super().__init__()

        self.sport_type = None
        self.workout_type = None

        print("Start parsing %s ..." % fit_file_name)
        # self.gpx_file = open(fit_file_name, 'r')
        fit_dictionary = self._parse_a_fit_file(fit_file_name)

        self.current_point_idx = 0
        self.points = fit_dictionary
        self.current_point = self.points[self.current_point_idx]
        self.current_track_name = fit_file_name

    def get_current_point(self):
        return self.points[self.current_point_idx]

    def get_next_point(self):
        self.current_point_idx += 1
        return self.points[self.current_point_idx]

    def close_file(self):
        return True

    def _parse_a_fit_file(self, filename, debug=False):
        # dataff = fp.FitFile('activities/1159050001.fit')
        dataff = fp.FitFile(filename, data_processor=fp.StandardUnitsDataProcessor())

        events = dataff.get_messages('event')
        file_id = dataff.get_messages('file_id')
        device_info = dataff.get_messages('device_info')
        sport = dataff.get_messages('sport')
        workout = dataff.get_messages('workout')
        record = dataff.get_messages('record')

        self.sport_type = sport
        self.workout_type = workout

        if debug:
            for info in events:
                fields = info.fields
                for field in fields:
                    print('%s: %s' % (field.name, field.value))

            for info in file_id:
                fields = info.fields
                for field in fields:
                    print('%s: %s' % (field.name, field.value))

            for info in device_info:
                fields = info.fields
                for field in fields:
                    print('%s: %s' % (field.name, field.value))

            for info in sport:
                fields = info.fields
                for field in fields:
                    print('%s: %s' % (field.name, field.value))

            for info in workout:
                fields = info.fields
                for field in fields:
                    print('%s: %s' % (field.name, field.value))

        list_of_dictionaries_with_data = []
        for record in dataff.get_messages('record'):
            # Go through all the data entries in this record
            this_dict = dict(lat=None, lon=None, ele=0, date=None, hr=0, cad=0, speed=0, atemp=None, dist=None)

            for record_data in record:
                # Print the records name and value (and units if it has any)
                # if record_data.units:
                #     print("%s: %s %s" % (record_data.name, record_data.value, record_data.units))
                # else:
                #     print("%s: %s" % (record_data.name, record_data.value))

                if record_data.name == 'timestamp' and record_data.value:
                    this_dict['date'] = record_data.value

                if record_data.name == 'enhanced_altitude' and record_data.value:
                    this_dict['ele'] = record_data.value

                if record_data.name == 'distance' and record_data.value:
                    this_dict['dist'] = record_data.value

                if record_data.name == 'grade' and record_data.value:
                    this_dict[record_data.name] = record_data.value

                if record_data.name == 'heart_rate':
                    hr_value = record_data.value
                    if not hr_value: hr_value = 0
                    this_dict['hr'] = hr_value

                if record_data.name == 'cadence':
                    cad_value = record_data.value
                    if not cad_value: cad_value = 0
                    this_dict['cad'] = cad_value

                if record_data.name == 'enhanced_speed':
                    speed_value = record_data.value
                    if not speed_value: speed_value = None
                    this_dict['speed'] = speed_value

                if 'temperature' in record_data.name:
                    temp_value = record_data.value
                    if not temp_value: temp_value = None
                    this_dict['atemp'] = temp_value

                if record_data.name == 'position_lat' and record_data.value:
                    latitude_semicircle = float(record_data.value)
                    latitude_degree = latitude_semicircle * (180. / 2 ** 31)
                    this_dict['lat'] = latitude_degree

                if record_data.name == 'position_long' and record_data.value:
                    latitude_semicircle = float(record_data.value)
                    latitude_degree = latitude_semicircle * (180. / 2 ** 31)
                    this_dict['lon'] = latitude_degree

            is_outdoor = this_dict['date'] and this_dict['lat'] and this_dict['lon']
            is_indoor = this_dict['date'] and this_dict['speed'] and this_dict['hr']
            if is_outdoor:
                # make sure you have lat long and date
                list_of_dictionaries_with_data.append(this_dict)
            elif is_indoor:
                # todo: save somewhere a flag that is an indoor training
                list_of_dictionaries_with_data.append(this_dict)
            else:
                print("Skipping point.")

        list_of_dictionaries_with_data.append('eof')
        return list_of_dictionaries_with_data


# fixme: in fit file missing cad, atemp
# return {'lat': lat, 'lon': lon, 'ele': ele, 'date': date, 'hr': hr, 'cad': cad, 'atemp': atemp}

# >> data_message.get_values()
# {
#     'altitude': 24.6,
#     'cadence': 97,
#     'distance': 81.97,
#     'grade': None,
#     'heart_rate': 153,
#     'position_lat': None,
#     'position_long': None,
#     'power': None,
#     'resistance': None,
#     'speed': 7.792,
#     'temperature': 20,
#     'time_from_course': None,
#     'timestamp': datetime.datetime(2011, 11, 6, 13, 41, 50)
# }


class FitParserAgain(DataContainer):
    def __init__(self, fit_file_name):
        super().__init__()

        self.sport_type = None
        self.workout_type = None

        self.allowed_fields = ['timestamp', 'position_lat', 'position_long', 'distance',
                              'enhanced_altitude', 'enhanced_speed', 'heart_rate', 'cadence', 'temperature']
        self.required_fields_outdoor = ['timestamp', 'position_lat', 'position_long']
        self.required_fields_indoor = ['timestamp', 'enhanced_speed']

        # start/stop events
        self.start_fields = ['timestamp', 'timer_trigger', 'event', 'event_type', 'event_group']
        self.start_required_fields = copy(self.start_fields)

        print("Start parsing %s ..." % fit_file_name)
        # self.gpx_file = open(fit_file_name, 'r')
        fit_dictionary = self._parse_a_fit_file(fit_file_name)

        self.current_point_idx = 0
        self.points = fit_dictionary
        self.current_point = self.points[self.current_point_idx]
        self.current_track_name = fit_file_name

    def get_current_point(self):
        return self.points[self.current_point_idx]

    def get_next_point(self):
        self.current_point_idx += 1
        return self.points[self.current_point_idx]

    def close_file(self):
        return True

    def get_timestamp(self, messages):
        for m in messages:
            fields = m.fields
            for f in fields:
                if f.name == 'timestamp':
                    return f.value
        return None

    def get_event_type(self, messages):
        for m in messages:
            fields = m.fields
            for f in fields:
                if f.name == 'sport':
                    return f.value
        return None

    def _parse_a_fit_file(self, filename, debug=False):
        dataff = fp.FitFile(filename, data_processor=fp.StandardUnitsDataProcessor())

        UTC = pytz.UTC

        list_of_dictionaries_with_data = []

        messages = dataff.messages

        # for m in messages:
        #     if not hasattr(m, 'fields'):
        #         continue
        #     fields = m.fields
        #
        #     # check for important data types
        #     mdata = {}
        #     for field in fields:
        #         if field.name in self.allowed_fields:
        #             if field.name == 'timestamp':
        #                 mdata[field.name] = UTC.localize(field.value).astimezone(UTC)
        #             else:
        #                 mdata[field.name] = field.value
#             for rf in required_fields_outdoor:
    #             if rf not in mdata:
    #                 skip = True
    #        if not skip:
    #             list_of_dictionaries_with_data.append(mdata)

        # 'timestamp', 'position_lat', 'position_long', 'distance', 'enhanced_altitude', 'altitude', 'enhanced_speed',
        # 'speed', 'heart_rate', 'cadence', 'fractional_cadence', 'temperature'
        translate_to_my_labels = {'position_lat': 'lat', 'position_long': 'lon', 'enhanced_altitude': 'ele',
                                  'timestamp': 'date', 'heart_rate': 'hr', 'cadence': 'cad', 'temperature': 'atemp',
                                  'enhanced_speed': 'speed', 'distance': 'dist'}
        list_of_dictionaries_with_data = []
        for m in messages:
            if not hasattr(m, 'fields'):
                continue
            fields = m.fields

            # check for important data types
            mdata = dict(lat=None, lon=None, ele=0, date=None, hr=0, cad=0, speed=0, atemp=None, dist=None)

            for field in fields:
                if field.name in self.allowed_fields:
                    if field.name == 'timestamp':
                        mdata[translate_to_my_labels[field.name]] = UTC.localize(field.value).astimezone(UTC)
                    else:
                        mdata[translate_to_my_labels[field.name]] = field.value

            is_indoor = sum([1 for rf in self.required_fields_indoor if mdata[translate_to_my_labels[rf]]]) == len(self.required_fields_indoor)
            is_outdoor = sum([1 for rf in self.required_fields_outdoor if mdata[translate_to_my_labels[rf]]]) == len(self.required_fields_outdoor)
            if is_outdoor:
                mdata['indoor'] = False
                list_of_dictionaries_with_data.append(mdata)
            if is_indoor:
                mdata['indoor'] = True
                list_of_dictionaries_with_data.append(mdata)

        list_of_dictionaries_with_data.append('eof')
        return list_of_dictionaries_with_data


