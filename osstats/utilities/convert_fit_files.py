import os


folder = 'activities'

files_in_folder = os.listdir(folder)

for filename in files_in_folder:
    if filename.endswith(".fit"):
        name, extension = os.path.splitext(filename)
        command = 'gpsbabel -i garmin_fit -f %s -o gpx -F %s.gpx' % (os.path.join(folder, filename), os.path.join(folder, name))
        os.system(command)
