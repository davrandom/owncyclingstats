import osstats.logs_statistics as st
import osstats.plot_stats as pt

import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as pyp
import matplotlib.patches as mpatches
import folium
import re
import pandas as pd
from datetime import datetime


""" Library of functions and classes to process and prepare the different outputs.
 Outputs can be maps, tables, text, ... """


def accumulate_to_pandas(accumulate_instance, out_path=''):
    """ Translate Accumulate class instance to a pandas DataStructure and save it to file.
        DS: I am definitely undecided on the quantities to save to file.
        We should get some more insight when making new plots with bokeh.
        """

    data = accumulate_instance
    timestamp_list = [time + data.t0 for time in data.absolute_time_list]
    date_list = [datetime.fromtimestamp(time) for time in timestamp_list]

    dictionary = {
        'latitude' :      pd.Series(data.lat_list, index=date_list),
        'longitude' :     pd.Series(data.lon_list, index=date_list),
        'elevation' :     pd.Series(data.ele_list, index=date_list),
        'time_step' :     pd.Series(data.delta_time_list, index=date_list),
        'distance_step':  pd.Series(data.distance_geo_list_m, index=date_list),
        'elevation_step': pd.Series(data.distance_ele_list_m, index=date_list),
        'speed_km_h' :    pd.Series(data.speed_km_h_list, index=date_list),
        'pace_s_km' :     pd.Series(data.pace_s_km, index=date_list),
        'trusted' :       pd.Series(data.trust_list, index=date_list),
        'stopped' :       pd.Series(data.stopped_list, index=date_list),
    }
    df = pd.DataFrame(dictionary)
    df.index = pd.to_datetime(df.index)  # convert index to datetime
    df.to_csv(os.path.join(out_path, data.name + '.cvs'))

    # It is possible (to evaluate if better) to use data.points_list
    # which is a list of dictionaries (dicts defined in parse_logs.py)
    # this way we could skip all the Accumulate class...
    # Example:
    points = data.points_list
    date_list = [point['date'] for point in points]
    dictionary = {
        'latitude' :   pd.Series([point['lat'] for point in points], index=date_list),
        'longitude' :  pd.Series([point['lon'] for point in points], index=date_list),
        'elevation' :  pd.Series([point['ele'] for point in points], index=date_list),
        'heart_rate':  pd.Series([point['hr'] for point in points], index=date_list),
        'cadence':     pd.Series([point['cad'] for point in points], index=date_list),
        'temperature': pd.Series([point['atemp'] for point in points], index=date_list)
    }
    df = pd.DataFrame(dictionary)
    df.index = pd.to_datetime(df.index)  # convert index to datetime
    df.to_csv(os.path.join(out_path, data.name + '_alternative.cvs'))

    return None


def summary(accumulate_instance, output_type='terminal'):
    """
    Produces two summary dictionaries from an instance of the Accumulate class.

    :param accumulate_instance: an instance of the accumulate class.
    :param output_type: where the output should be written.
    :return: dict_summary_total, dict_summary_moving, string_pauses.
    """
    a = accumulate_instance

    # Summaries
    # remove from calculations points you don't trust
    trust_list = np.array(a.trust_list)
    delta_time_list = np.array(a.delta_time_list)
    distance_geo_list_m = np.array(a.distance_geo_list_m)
    distance_ele_list_m = np.array(a.distance_ele_list_m)
    speed_km_h_list = np.array(a.speed_km_h_list)
    pace_s_km = np.array(a.pace_s_km)

    # raw data
    total_distance_geodesic = float(np.sum(distance_geo_list_m))
    total_distance_with_ele = float(np.sum(distance_ele_list_m))

    # corrected (compare with raw)
    moving_tot_distance_geo = float(np.sum(distance_geo_list_m[trust_list]))
    moving_tot_distance_ele = float(np.sum(distance_ele_list_m[trust_list]))

    # Total
    dict_summary_total = {}
    dict_summary_total['total'] = ["Summary (total):", "value", "unit"]

    start_date = a.points_list[0]['date']
    stop_date = a.points_list[-1]['date']
    total_elapsed_time_date = stop_date - start_date
    total_elapsed_time_s = total_elapsed_time_date.total_seconds()
    total_elapsed_time_min_sec = st.from_s_to_h_m_s(total_elapsed_time_s)

    total_avg_speed_m_s = moving_tot_distance_geo / max(total_elapsed_time_s, 1)
    total_avg_speed_km_h = total_avg_speed_m_s * 3.6
    total_pace_s_km = total_elapsed_time_s / max(moving_tot_distance_geo / 1000, 1)
    total_pace_min_km = st.from_pace_s_km_to_pace_min_km(total_pace_s_km)

    dict_summary_total['total_distance']  = ["Total distance:", "%.2f" % (moving_tot_distance_geo / 1000), "km"]
    dict_summary_total['total_time']      = ["Total time:", "%d:%d:%02d" % total_elapsed_time_min_sec, "hh:mm:ss"]
    dict_summary_total['total_avg_speed'] = ["Average speed:", "%.1f" % total_avg_speed_km_h, "km/h"]
    dict_summary_total['total_avg_pace']  = ["Average pace:", "%d:%02d" % total_pace_min_km, "min/km"]
    dict_summary_total['date'] = [start_date, stop_date]

    # Moving
    dict_summary_moving = {}
    dict_summary_moving['moving'] = ["Summary (moving):", "value", "unit"]

    moving_time = np.sum(delta_time_list[trust_list])
    moving_time_min_sec = st.from_s_to_h_m_s(moving_time)
    moving_avg_speed_km_h = np.mean(speed_km_h_list[trust_list])
    moving_avg_pace_s_km = np.mean(pace_s_km[trust_list])
    moving_avg_pace_min_km = st.from_pace_s_km_to_pace_min_km(moving_avg_pace_s_km)

    # fixme: move this somewhere else >>>
    elevation_list = np.array(a.ele_list)
    trusted_elevation_list = elevation_list[trust_list]
    ele_diffs = [trusted_elevation_list[idx+1] - trusted_elevation_list[idx] for idx in range(len(trusted_elevation_list)-1)]
    positive_ele_diffs = [el for idx, el in enumerate(ele_diffs) if el > 0]
    # fixme: move this somewhere else <<<

    accumulated_elevation_in_meters = np.sum(positive_ele_diffs)

    dict_summary_moving['moving_distance']  = ["Total distance:", "%.2f" % (moving_tot_distance_geo / 1000.), "km"]
    dict_summary_moving['moving_time']      = ["Moving time:", "%d:%d:%02d" % moving_time_min_sec, "hh:mm:ss"]
    dict_summary_moving['moving_avg_speed'] = ["Average moving speed:", "%.1f" % moving_avg_speed_km_h, "km/h"]
    dict_summary_moving['moving_avg_pace']  = ["Average moving pace:", "%d:%02d" % moving_avg_pace_min_km, "min/km"]
    dict_summary_moving['elevation_estimation'] = ["Total estimated accumulated elevation:", "%d" % accumulated_elevation_in_meters, "meters"]
    dict_summary_moving['heart_rate_mean'] = ["Mean heart rate:", "%d" % np.mean(a.hr_list), "bpm"]

    # Pauses
    string_pauses = ''
    how_long = delta_time_list[np.logical_not(trust_list)]
    string_pauses += "Application paused %d times: \n" % len(how_long)
    for idx, pauselength in enumerate(how_long):
        h, m, s = st.from_s_to_h_m_s(pauselength)
        string_pauses += "%d) %d:%02d \n" % (idx+1, m, s)

    how_long = delta_time_list[a.not_moving_list]  # this might work by chance (by the fact that app is autopausing)
    string_pauses += "You made %d pauses: \n" % len(how_long)
    for idx, pauselength in enumerate(how_long):
        h, m, s = st.from_s_to_h_m_s(pauselength)
        string_pauses += "%d) %d:%02d \n" % (idx+1, m, s)

    # experimenting
    stopped_list = np.array(a.stopped_list)
    stopped_s = np.sum(delta_time_list[stopped_list])
    stopped_h_m_s = st.from_s_to_h_m_s(stopped_s)

    return dict_summary_total, dict_summary_moving, string_pauses


def gpx_map(accumulate_instance, output_type='mthl4markdown', out_path=''):
    a = accumulate_instance

    lat_center = sum(lat for lat in a.lat_list) / len(a.lat_list)
    lon_center = sum(lon for lon in a.lon_list) / len(a.lon_list)
    points_list = [tuple([lat, a.lon_list[idx]]) for idx, lat in enumerate(a.lat_list)]

    # Load map centred on average coordinates
    my_map = folium.Map(location=[lat_center, lon_center], zoom_start=14)

    # add markers
    start_point = points_list[0]
    end_point = points_list[-1]

    start_icon = folium.map.Icon(color='green', icon_color='white', icon='play')
    stop_icon  = folium.map.Icon(color='red', icon_color='white', icon='stop')
    folium.Marker(start_point, popup='Start', icon=start_icon).add_to(my_map)
    folium.Marker(end_point, popup='End', icon=stop_icon).add_to(my_map)

    # fadd lines
    folium.PolyLine(points_list, color="red", weight=2.5, opacity=1).add_to(my_map)

    # get html
    html_string = my_map.get_root().render()

    # write to standalone html file
    if out_path:
        html_handle = open(os.path.join(out_path, 'map.html'), 'w')  # TODO add unique name
        html_handle.write(html_string)
        html_handle.close()

    # hacky parsing of html
    lines = html_string.split('\n')
    # ignore everything in lines until <body>
    interesting_lines = []
    trash = True
    inside_style = False
    for line in lines:
        if '<style>' == line.replace(' ', ''):
            trash = False
            inside_style = True
        if '</style>' == line.replace(' ', ''):
            inside_style = False

        if 'body>' in line:
            trash = False  # skips everything before body
            interesting_lines.append('\n')
            continue  # skips body closure

        if 'head>' in line:
            continue

        if not trash:
            add_line = line
            if 'var ' in line or '<div ' in line or 'style>' in line:
                add_line = line.strip()

            if inside_style and 'height: 100.0%;' in line:
                # replace height in percentage to height in px
                add_line = ' height: 400px;'

            if add_line:
                add_line = re.sub(' +', ' ', add_line)
                add_line = add_line.rstrip()
                interesting_lines.append(add_line)

    return interesting_lines


def figures_default_set(accumulate_instance, out_path=''):
    a = accumulate_instance

    delta_time_list = np.array(a.delta_time_list)
    speed_km_h_list = np.array(a.speed_km_h_list)
    trust_list = np.array(a.trust_list)

    # PLOTTING
    # plotting raw stuff
    # pt.simple_plot_variable(a.ele_list, 'elevation', out_path)
    pt.simple_plot_two_variables([idx for idx in range(len(a.ele_list))], a.ele_list, 'elevation', 'point index', 'meters', out_path)
    pt.simple_plot_two_variables([idx for idx in range(len(delta_time_list))], delta_time_list, 'delta time', 'point index', 'seconds', out_path)

    pt.simple_plot_two_variables(a.absolute_time_list, a.ele_list, 'elevation (meters)', 'seconds', 'meters', out_path)
    pt.simple_plot_two_variables(a.absolute_time_list, a.distance_geo_list_m, 'distance (meters)', 'seconds', 'meters', out_path)
    pt.simple_plot_two_variables(a.absolute_time_list, a.speed_m_s_list, 'speed (m/s)', 'seconds', 'm/s', out_path)
    pt.simple_plot_two_variables(a.absolute_time_list, a.speed_km_h_list, 'speed (km/h)', 'seconds', 'km/h', out_path)
    pt.simple_plot_two_variables(a.absolute_time_list, a.accel_list, 'acceleration (m/s^2)', 'seconds', 'm/s^2', out_path)
    pt.simple_plot_two_variables(a.absolute_time_list, a.cad_list, 'cadence (rmp)', 'seconds', 'rounds per minute or steps per minute', out_path)
    pt.simple_plot_two_variables(a.absolute_time_list, a.hr_list, 'heart rate (bpm)', 'seconds', 'beats per minute', out_path)

    # highlighting "wrong" points (tbd)
    absolute_time_list = np.array(a.absolute_time_list)
    not_moving_list = np.array(a.not_moving_list)

    var_name = 'speed'
    x = np.array(absolute_time_list)
    y = np.array(speed_km_h_list)
    x_label = 'seconds'
    y_label = 'm/s'

    fig, ax = pyp.subplots()
    pyp.plot(x, y)
    if var_name:
        pyp.legend([var_name])
    else:
        var_name = 'default_name'
    if x_label:
        pyp.xlabel(x_label)
    if y_label:
        pyp.ylabel(y_label)

    # pyp.plot(x, trust_list)
    # pyp.plot(x[np.logical_not(trust_list)], y[np.logical_not(trust_list)])
    where_stop = absolute_time_list[np.logical_not(trust_list)]
    how_long = delta_time_list[np.logical_not(trust_list)]
    for idx, whe in enumerate(where_stop):
        patch = mpatches.Rectangle((whe-how_long[idx], np.min(y)), how_long[idx], np.max(y)*1.1+0.1, angle=0.0, color='r', ec="none", alpha=0.3)
        #    Draw a rectangle with lower left at xy = (x, y) with specified width, height and rotation angle.
        ax.add_patch(patch)

    where_nm = absolute_time_list[not_moving_list]
    how_long = delta_time_list[not_moving_list]
    for idx, whe in enumerate(where_nm):
        patch = mpatches.Rectangle((whe-how_long[idx], np.min(y)), how_long[idx], np.max(y)*1.1+0.1, angle=0.0, color='g', ec="none", alpha=0.3)
        #    Draw a rectangle with lower left at xy = (x, y) with specified width, height and rotation angle.
        ax.add_patch(patch)

    pyp.axis('auto')
    pyp.tight_layout()

    # pyp.show()
    pyp.savefig(os.path.join(out_path, var_name + '.png'))
    pyp.close()


def get_missing_bokeh_html_lines(html_string):
    lines = html_string.split('\n')
    maybe = []
    how_many_lines = 0
    for line in lines:
        is_interesting = 'script type="application/json"' in line
        if is_interesting: how_many_lines = 3
        if is_interesting or how_many_lines:
            maybe.append(line.lstrip())
            how_many_lines -= 1
    maybe = '\n'.join(maybe)
    return maybe


def bokeh_figures_simple_example(accumulate_instance, out_path=''):
    a = accumulate_instance

    script, div, html = pt.bokeh_simple_plot_two_variables(a.absolute_time_list, a.ele_list, 'elevation (meters)', 'seconds', 'meters', out_path)

    maybe = get_missing_bokeh_html_lines(html)

    output_string = div
    output_string += '\n' + maybe + '\n'
    output_string += '\n' + script + '\n'
    return output_string


def bokeh_figures_default_set(accumulate_instance, out_path=''):
    a = accumulate_instance

    output_string = ''

    list_of_dicts = []
    dict_ele = {'x': a.absolute_time_list, 'y': a.moving_ele_list, 'var_name': 'elevation (meters)', 'x_label': 'seconds', 'y_label': 'meters'}
    list_of_dicts.append(dict_ele)

    if any([hr > 10 for hr in a.hr_list]):
        dict_hr = {'x': a.absolute_time_list, 'y': a.moving_hr_list, 'var_name': 'heart-rate (beats-per-minute)', 'x_label': 'seconds', 'y_label': 'beats-per-minute'}
        list_of_dicts.append(dict_hr)

    dict_speed = {'x': a.absolute_time_list, 'y': a.moving_speed_km_h_list, 'var_name': 'speed (km/h)', 'x_label': 'seconds', 'y_label': 'km/h'}
    list_of_dicts.append(dict_speed)
    # OR append pace if you are running...

    script, div, html = pt.bokeh_multiplot_from_list_dictionary(list_of_dicts, title='speed and elevation plot', out_path=out_path)

    maybe = get_missing_bokeh_html_lines(html)

    output_string = div
    output_string += '\n' + maybe + '\n'
    output_string += '\n' + script + '\n'

    return output_string


class WriteToMarkdownFile:
    def __init__(self, filename):
        self.filehandle = open(filename, 'w+')

    def header_jekyll(self, layout='post', title='Title', category='', comments='true'):
        self.write_this('---\n')
        self.write_this('layout: %s\n' % layout)
        self.write_this('title: "%s"\n' % title)
        self.write_this('categories: %s\n' % category)
        self.write_this('tags: %s\n' % category)
        self.write_this('comments: %s\n' % comments)
        self.write_this('---\n\n')

    def header_pelican(self, title='Title', date='', category=''):
        self.write_this('---\n')
        self.write_this('Title: %s\n' % title)
        self.write_this('Date: %s\n' % date)
        self.write_this('Category: %s\n' % category)
        self.write_this('---\n\n')

    def close(self):
        self.filehandle.close()

    def write_this(self, string):
        self.filehandle.write(string)

    @staticmethod
    def make_table_out_of_dictionary(dictionary):
        values = dictionary.values()
        table = ''
        for idx, val in enumerate(values):
            if len(val) == 3:
                table += "| %s | %s | %s | \n" % (val[0], val[1], val[2])
                if idx == 0:
                    table += "| :--- | ---: | :--- | \n"

        return table


def print_dictionary(dictionary):
    values = dictionary.values()
    for val in values:
        if len(val) == 3:
            print("%s \t%s \t%s" % (val[0], val[1], val[2]))

    print("\n")


def write_dictionary_to_txt_file(list_of_dictionaries, filename):
    file_handle = open(filename, 'w')

    for dictionary in list_of_dictionaries:
        values = dictionary.values()
        for val in values:
            if len(val) == 3:
                file_handle.write("%s \t%s \t%s\n" % (val[0], val[1], val[2]))

    file_handle.write('\n')
    file_handle.close()


def read_txt_file_to_dictionary(filename):
    file_handle = open(filename, 'r')
    dictionary = {}
    for line in file_handle:
        name, value, unit = line.split('\t')
        name = name.replace(': ', '')
        unit = unit.replace('\n', '')
        dictionary[name] = [value, unit]

    file_handle.close()
    return dictionary



