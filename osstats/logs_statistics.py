from geopy.distance import geodesic
import numpy as np
import math


class InstantDerivedQuantities:
    def __init__(self, point):
        self.observable_dict_t0 = {}
        self.observable_dict_t1 = {}
        self.distance_km = 0
        self.distance_m = 0
        self.distance_w_ele = 0
        self.speed_m_s = 0
        self.speed_km_h = 0
        self.pace_s_km = 0
        self.pace_min_km = 0
        self.accel_m_ss = 0
        self.timestamp_s = point['date'].timestamp()
        self.elapsed_time = 0

        self.trust = False
        self.stopped = False
        self.not_moving = False

        self.moving_avg_window = 5
        self.moving_avg_speed_km_h = []
        self.moving_accumulated_distance = []
        self.moving_elevation = []
        self.moving_hr = []

        self.point_idx = 0

    def push_new_observable_list(self, observable_dict):
        # copy "old" dict into t0
        self.observable_dict_t0 = self.observable_dict_t1
        # put new dict into t1
        self.observable_dict_t1 = observable_dict

    def update_moving_variable(self, variable, value):
        # fill moving average container
        if len(variable) < self.moving_avg_window:
            variable.append(value)
        else:
            variable.pop(0)
            variable.append(value)
        return variable

    def push_and_calculate(self, observable_dict):
        old_timestamp = self.timestamp_s
        self.push_new_observable_list(observable_dict)
        self.timestamp_s = self.observable_dict_t1['date'].timestamp()
        self.elapsed_time = (self.timestamp_s - old_timestamp)

        if observable_dict['lat'] and observable_dict['lon']:
            # use latitude and longitude
            self.distance_km = self.calc_distance_geodesic()
            self.distance_m = self.distance_km * 1000
            self.distance_w_ele = self.calc_distance_with_elevation()
            self.speed_m_s, self.speed_km_h = self.calc_speed_geodesic()
            self.pace_s_km, self.pace_min_km = self.calc_pace_geodesic()

        elif observable_dict['dist']:
            self.distance_m = observable_dict['dist']
            self.distance_km = self.distance_m / 1000
            self.distance_w_ele = self.distance_m
            self.speed_m_s, self.speed_km_h = self.calc_speed_geodesic()
            self.pace_s_km, self.pace_min_km = self.calc_pace_geodesic()

        elif observable_dict['speed']:
            # observable_dict['speed'] in km/h
            self.distance_m = observable_dict['speed'] / 3.6 * self.elapsed_time
            self.distance_km = self.distance_m / 1000
            self.distance_w_ele = self.distance_m

            self.speed_km_h = observable_dict['speed']
            self.speed_m_s = observable_dict['speed'] / 3.6
            self.pace_s_km, self.pace_min_km = self.calc_pace_geodesic()

        else:
            raise ValueError("Can't calculate the distance.")

        self.stopped = False
        self.not_moving = False
        if self.speed_m_s < 30:
            self.trust = True
        if self.elapsed_time > 15:  # assuming you take a point more often than every 5 seconds
            self.stopped = True

        # fill moving average container
        self.moving_avg_speed_km_h = self.update_moving_variable(self.moving_avg_speed_km_h, self.speed_km_h)
        self.moving_accumulated_distance = self.update_moving_variable(self.moving_accumulated_distance, self.distance_m)
        self.moving_elevation = self.update_moving_variable(self.moving_elevation, observable_dict['ele'])
        self.moving_hr = self.update_moving_variable(self.moving_hr, observable_dict['hr'])

        # calculate average speed and decide if moving
        avg_speed_km_h = sum(self.moving_avg_speed_km_h) / len(self.moving_avg_speed_km_h)

        # calculated accumulated distance
        accumulated_distance_in_window = sum(self.moving_accumulated_distance)
        if avg_speed_km_h < 0.5 and accumulated_distance_in_window < 1.0:  # FIXME put this limit on the moving-average speed, to stay on the safe side
            self.not_moving = True

        if self.stopped or self.not_moving:
            self.trust = False

        self.point_idx += 1

    # distance
    def calc_distance_geodesic(self, units=0):
        # units = 0 metric
        # units = 1 imperial
        pt0 = (self.observable_dict_t0['lat'], self.observable_dict_t0['lon'])
        pt1 = (self.observable_dict_t1['lat'], self.observable_dict_t1['lon'])

        distance = haversine(pt0, pt1)  # in meters
        if units:
            distance = geodesic(pt0, pt1).miles
            # distance = distance / 1000 * 0.621371  # in miles
        else:
            distance = geodesic(pt0, pt1).km
            # distance = distance / 1000  # in km
        return distance

    def calc_distance_with_elevation(self, units=0):
        lat_rad_pt0 = self.observable_dict_t0['lat'] * np.pi / 180.0
        lon_rad_pt0 = self.observable_dict_t0['lon'] * np.pi / 180.0

        lat_rad_pt1 = self.observable_dict_t1['lat'] * np.pi / 180.0
        lon_rad_pt1 = self.observable_dict_t1['lon'] * np.pi / 180.0

        earth_mean_radius = 6371 * 1000  # in meters

        r_pt0 = earth_mean_radius
        if 'ele' in self.observable_dict_t0.keys():
            r_pt0 += self.observable_dict_t0['ele']

        x_pt0 = r_pt0 * np.sin(lon_rad_pt0) * np.cos(lat_rad_pt0)
        y_pt0 = r_pt0 * np.sin(lon_rad_pt0) * np.sin(lat_rad_pt0)
        z_pt0 = r_pt0 * np.cos(lon_rad_pt0)

        r_pt1 = earth_mean_radius
        if 'ele' in self.observable_dict_t1.keys():
            r_pt1 += self.observable_dict_t1['ele']

        x_pt1 = r_pt1 * np.sin(lon_rad_pt1) * np.cos(lat_rad_pt1)
        y_pt1 = r_pt1 * np.sin(lon_rad_pt1) * np.sin(lat_rad_pt1)
        z_pt1 = r_pt1 * np.cos(lon_rad_pt1)

        distance = np.sqrt((x_pt1 - x_pt0)**2 + (y_pt1 - y_pt0)**2 + (z_pt1 - z_pt0)**2)  # in meters
        distance = distance / 1000.0  # in km
        # TDB in miles
        return distance

    # speed
    def calc_speed_geodesic(self):
        time_pt0 = self.observable_dict_t0['date'].timestamp()
        time_pt1 = self.observable_dict_t1['date'].timestamp()

        speed_m_s   = 0.
        speed_km_h  = 0.

        delta_t = float(time_pt1 - time_pt0)
        if delta_t > 0.:
            speed_m_s = self.distance_km * 1000.0 / float(delta_t)
            speed_km_h = speed_m_s * 3.6

        return speed_m_s, speed_km_h

    def calc_pace_geodesic(self):
        time_pt0 = self.observable_dict_t0['date'].timestamp()
        time_pt1 = self.observable_dict_t1['date'].timestamp()

        pace_s_km   = 0.
        pace_min_km = 0.
        if self.distance_km > 0.:
            pace_s_km = float(time_pt1 - time_pt0) / self.distance_km
            pace_min_km = from_pace_s_km_to_pace_min_km(pace_s_km)  # (minutes, seconds)

        return pace_s_km, pace_min_km


def from_pace_s_km_to_pace_min_km(pace_s_km):
    try:
        minutes = int(np.floor(pace_s_km / 60))
    except Exception:
        minutes = 0
    try:
        seconds = pace_s_km % 60
    except Exception:
        seconds = 0

    if math.isnan(minutes):
        minutes = 0
    if math.isnan(seconds):
        seconds = 0

    pace_min_km = (minutes, seconds)
    return pace_min_km


def from_s_to_h_m_s(seconds):
    h_m_s = ( int(seconds / 3600), int(seconds / 60) % 60, seconds % 60)
    return h_m_s


class Accumulate:
    def __init__(self):
        self.name = 'activity_name'

        # lists
        self.points_list = []
        self.delta_time_list = []
        self.absolute_time_list = []
        self.lat_list = []
        self.lon_list = []
        self.ele_list = []
        self.hr_list = []
        self.cad_list = []

        self.trust_list = []

        self.distance_geo_list_m = []
        self.distance_ele_list_m = []
        self.speed_m_s_list = []
        self.speed_km_h_list = []
        self.pace_s_km = []
        self.pace_min_km = []
        self.accel_list = []

        self.stopped_list = []
        self.not_moving_list = []

        # scalars
        self.t0 = 0

        # moving average
        self.moving_ele_list = []
        self.moving_hr_list = []
        self.moving_speed_km_h_list = []

    def set_initial_timestamp(self, t0):
        self.t0 = t0

    def push_original_data(self, point):
        # for offline analysis
        self.points_list.append(point)

        # offline stuff
        # primal data
        lat = point['lat']
        self.lat_list.append(lat)
        lon = point['lon']
        self.lon_list.append(lon)
        ele = point['ele']
        self.ele_list.append(ele)

        hr = point['hr']
        self.hr_list.append(hr)
        cad = point['cad']
        self.cad_list.append(cad)

        date = point['date']
        self.absolute_time_list.append(date.timestamp()-self.t0)

    def push_derived_quantities(self, der):
        # der instance of InstantDerivedQuantities

        # instantaneous derived quantities
        self.delta_time_list.append(der.elapsed_time)

        self.trust_list.append(der.trust)
        self.stopped_list.append(der.stopped)
        self.not_moving_list.append(der.not_moving)

        self.distance_geo_list_m.append(der.distance_m)
        self.distance_ele_list_m.append(der.distance_w_ele)
        self.speed_m_s_list.append(der.speed_m_s)
        self.speed_km_h_list.append(der.speed_km_h)
        self.accel_list.append(der.accel_m_ss)
        self.pace_s_km.append(der.pace_s_km)
        self.pace_min_km.append(der.pace_min_km)

        # moving average
        self.moving_ele_list.append(sum(der.moving_elevation)/len(der.moving_elevation))
        self.moving_hr_list.append(sum(der.moving_hr)/len(der.moving_hr))
        self.moving_speed_km_h_list.append(sum(der.moving_avg_speed_km_h)/len(der.moving_avg_speed_km_h))

    @staticmethod
    def list_to_array(alist):
        return np.array(alist)


def haversine(coord1, coord2):
    # from https://janakiev.com/blog/gps-points-distance-python/
    R = 6372800  # Earth radius in meters
    lat1, lon1 = coord1
    lat2, lon2 = coord2

    phi1, phi2 = math.radians(lat1), math.radians(lat2)
    dphi = math.radians(lat2 - lat1)
    dlambda = math.radians(lon2 - lon1)

    a = math.sin(dphi / 2) ** 2 + \
        math.cos(phi1) * math.cos(phi2) * math.sin(dlambda / 2) ** 2

    return 2 * R * math.atan2(math.sqrt(a), math.sqrt(1 - a))


